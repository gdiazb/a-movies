# A-Movies

Proyecto desarrollado con vue.js, usa plugins como hopper slider, vuetify, y toda la información de las peiculas fué extraica del Api: de The Movie DB.

Estilso codificados con el preprocesador SASS y usando la sintaxis SCSS

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Hecho por Geraldine Díaz para Ariadna Communications Group
